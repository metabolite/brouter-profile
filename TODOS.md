## Common
* Write Documentation
* Penalty depending on kerb type (kerb=raised/lowered/flush) - requires brouter to include it first.
* is_sidepath penalty - requires brouter to include it first.
* left sidepath penalty - depends on is_sidepath
* Schutzstreifen penalty - requires brouter to include it first (in progress, see [Brouter Issue #416](https://github.com/abrensch/brouter/issues/416).
* destination access only as initial cost, don't multiply with way length
* steps penalty not multiplied by distance
* Reduce too high cost factors as doc says "the profile should be able to find a route with an average costfactor
  not very much larger than one, because otherwise the routing algorithm
  will not find a reasonable cost-cutoff, leading to a very large
  search area and thus to long processing times."
  https://brouter.de/brouter/profile_developers_guide.txt

## CM
* Avoid streets with trams

## New Profiles
* Relaxed Cycling
